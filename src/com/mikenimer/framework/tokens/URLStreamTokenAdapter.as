package com.mikenimer.framework.tokens
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.GenericAsyncToken;
	import mx.rpc.IResponder;
	
	public class URLStreamTokenAdapter
	{
		private var _asyncToken:GenericAsyncToken;
		private var _urlStream : URLStream;
		
		protected var _resp : IResponder;
		
		[Bindable]
		public function get urlStream() : URLStream
		{
			return _urlStream;
		}
		
		public function set urlStream( value : URLStream ) : void
		{
			_urlStream = value;
		}		

		public function load(request : URLRequest):AsyncToken
		{	
			if( _urlStream == null )
			{
				_urlStream = new URLStream();
			}
			
	        _urlStream.addEventListener(Event.COMPLETE, resultHandler, false, 0, false);
			_urlStream.addEventListener(IOErrorEvent.IO_ERROR , errorHandler, false, 0, false);
	        _urlStream.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler, false, 0, false);
	 			 	
			_urlStream.load( request);
			
			_asyncToken = new GenericAsyncToken(null);
			_asyncToken.addResponder( _resp );
			return _asyncToken;			
		}
		
		
		protected function resultHandler(event:Event):void
		{
			_asyncToken.applyGenericResult(event);	
		}
		
		
		protected function errorHandler(event:Event):void
		{
			_asyncToken.applyGenericFault(event);	
		}


		public function URLStreamTokenAdapter( _resp : IResponder )
		{
			this._resp = _resp;
		}
	}
}
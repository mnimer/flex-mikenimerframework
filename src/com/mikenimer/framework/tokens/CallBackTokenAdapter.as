package com.mikenimer.framework.tokens
{
	import flash.net.Responder;
	
	import mx.core.mx_internal;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	use namespace mx_internal;

	/**Class intended to work around to ridiculous use of responders in the SQL calls.
	 * Currently, to use the responder notification, you need to pass it an instance of
	 * a responder class instead of using the async token methodology in all other
	 * async calls. This class attempts to remedy that issue
	 **/
	public class CallBackTokenAdapter extends Responder
	{
		public var asyncToken:AsyncToken;

		/**
		 *  This method is called by SQL when a result has been received.
		 */
		public function handleAsyncResult(data:Object):void
		{
			asyncToken.applyResult( data as ResultEvent );
		}
		
		/**
		 *  This method is called by SQL when an error has been received.
		 */
		public function handleAsyncResultFault(info:Object):void
		{
			asyncToken.applyFault( info as FaultEvent );
		}

	
		public function CallBackTokenAdapter()
		{
			asyncToken = new AsyncToken( null );
			super( handleAsyncResult, handleAsyncResultFault );
		}
	}
}
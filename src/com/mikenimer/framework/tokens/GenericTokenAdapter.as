package com.mikenimer.framework.tokens
{
	import flash.events.Event;
	
	import mx.messaging.messages.AbstractMessage;
	import mx.messaging.messages.IMessage;
	import mx.rpc.GenericAsyncToken;
	
	public class GenericTokenAdapter
	{
		public var asyncToken:GenericAsyncToken;


		public function GenericTokenAdapter()
		{
			var message:IMessage = new AbstractMessage();
			asyncToken = new GenericAsyncToken(message);
		}

		
		protected function resultHandler(event:Object):void
		{
			asyncToken.applyGenericResult(event);	
		}
		
		
		protected function faultHandler(event:Object):void
		{
			asyncToken.applyGenericFault(event);	
		}

	}
}
package com.mikenimer.framework.adapters
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.GenericAsyncToken;
	import mx.rpc.IResponder;
	
	public class URLLoaderAdapter
	{
		private var _asyncToken:GenericAsyncToken;
		private var _urlLoader:URLLoader;
		private var _dataFormat:String = "text";
		
		protected var _resp : IResponder;

		[Bindable]
		public function get dataFormat() : String
		{
			return _dataFormat;
		}
		
		public function set dataFormat( value : String ) : void
		{
			_dataFormat = value;
		}
		
		[Bindable]
		public function get urlLoader() : URLLoader
		{
			return _urlLoader;
		}
		
		public function set urlLoader( value : URLLoader ) : void
		{
			_urlLoader = value;
		}		

		public function load(request:URLRequest):AsyncToken
		{	
			if( _urlLoader == null )
			{
				_urlLoader = new URLLoader();
			}
			
	        _urlLoader.addEventListener(Event.COMPLETE, resultHandler, false, 0, false);
			_urlLoader.addEventListener(IOErrorEvent.IO_ERROR , errorHandler, false, 0, false);
	        _urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler, false, 0, false);
	        
	        _urlLoader.dataFormat = _dataFormat;
	 			 	
			_urlLoader.load(request);
			
			_asyncToken = new GenericAsyncToken(null);
			_asyncToken.addResponder( _resp );
			return _asyncToken;			
		}
		
		
		protected function resultHandler(event:Event):void
		{
			_asyncToken.applyGenericResult(event);	
		}
		
		
		protected function errorHandler(event:Event):void
		{
			_asyncToken.applyGenericFault(event);	
		}


		public function URLLoaderAdapter( _resp : IResponder )
		{
			this._resp = _resp;
		}
	}
}
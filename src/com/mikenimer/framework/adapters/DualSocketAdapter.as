package com.mikenimer.framework.adapters
{
	import flash.events.Event;
	import flash.net.XMLSocket;
	
	import mx.logging.Log;
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.XMLAsyncToken;
	import mx.utils.UIDUtil;

	public class DualSocketAdapter extends SocketAdapter
	{
		private var _outgoingSocket:XMLSocket;
		private var _outgoingHost:String;
		private var _outgoingPort:int;
		private var _outgoingAvailable:Boolean;
		private var _incomingHost:String;
		private var _incomingPort:int;
		private var _incomingSocket:XMLSocket;
		private var _incomingAvailable:Boolean;
		
		[Bindable]
		public function get outgoingSocket() : XMLSocket
		{
			return _outgoingSocket;
		}
		
		public function set outgoingSocket( value : XMLSocket ) : void
		{
			_outgoingSocket = value;
		}	
		[Bindable]
		public function get incomingSocket() : XMLSocket
		{
			return _incomingSocket;
		}
		
		public function set incomingSocket( value : XMLSocket ) : void
		{
			incomingSocket = value;
		}	
		public function establishOutgoingSockets(hostName:String, port:int):void{
			_outgoingHost = hostName;
			_outgoingPort = port;
			_outgoingSocket = new XMLSocket();
			configureListeners(_outgoingSocket);
            _outgoingSocket.connect(hostName, port);
  		}
		public function establishIncomingSockets(hostName:String, port:int):void{
			_incomingHost = hostName;
			_incomingPort = port;
			_incomingSocket = new XMLSocket();
			configureListeners(_incomingSocket);
            _incomingSocket.connect(hostName, port);
		}
		public override function sendData(msg:XML,resp:IResponder):AsyncToken{
			
			var tokenId:String = UIDUtil.createUID();
			msg.@token = tokenId;
			var token:XMLAsyncToken = new XMLAsyncToken(msg);
			token.addResponder(resp);
			tokenDict[tokenId] = token;
			Log.getLogger("services").info("sending message:"+msg.toXMLString());
			if(_outgoingAvailable && _incomingAvailable){
				outgoingSocket.send(msg.toXMLString());
			} else {
				msgQueue.push(msg);
				trace("socket not yet available, adding to queue");
			}
			return token;
		}
		private function runQueue():void{
			if(_outgoingAvailable && _incomingAvailable){
				while(msgQueue.length){
					trace("sending message:"+msgQueue.length);
					var msg:XML = msgQueue.shift();
					outgoingSocket.send(msg.toXMLString());
				}
				trace("end of queue");
			}
			
		}
		public override function establishSocket(hostName:String, port:int):void{
			throw new Error("establishSocket is only available from SocketAdapter, not DualSocketAdapter.  Try using establishIncomingSocket and establishOutgoingSocket instead");
  		}
		
		
		private function handleDisconnect(socket:XMLSocket):void{
			var host:String;
			var port:int;
			if(socket == outgoingSocket){
				socket = outgoingSocket;
				host = _outgoingHost;
				port = _outgoingPort;
				_outgoingAvailable = false;
			} else if (socket == incomingSocket){
				socket = incomingSocket;
				host = _incomingHost;
				port = _incomingPort;
				_incomingAvailable = false;
			}
			if(_autoReconnect){
				 tryReconnect(socket,host,port);
			}
		}
		private function connectHandler(event:Event):void{
			if(event.target == _outgoingSocket){
				_outgoingAvailable = true;
			
			} else if (event.target == _incomingSocket){
				_incomingAvailable = true;
			}
				runQueue();
		}
		
		public function DualSocketAdapter(autoReconnect:Boolean=false)
		{
			
			_autoReconnect = autoReconnect; 

		}

	}
}
package com.mikenimer.framework.adapters
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.URLStream;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.GenericAsyncToken;
	import mx.rpc.IResponder;

	public class FileUploadAdapter
	{
		public function FileUploadAdapter(_resp : IResponder )
		{
			this._resp = _resp;
		}
		
		private var _asyncToken:GenericAsyncToken;
		protected var _resp : IResponder;
		protected var _file:File;
		
		public function get file():File
		{
			return _file;
		}
		
		public function set file(f:File):void
		{
			_file = f;
		}
		
	
		public function upload( urlReq_:URLRequest, fieldName_:String=null ):AsyncToken
		{	
			_file.addEventListener(Event.COMPLETE, resultHandler, false, 0, false);
			_file.addEventListener(IOErrorEvent.IO_ERROR , errorHandler, false, 0, false);
			_file.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler, false, 0, false);
			
			_file.upload( urlReq_, fieldName_ );
			
			_asyncToken = new GenericAsyncToken(null);
			_asyncToken.addResponder( _resp );
			return _asyncToken;			
		}
		
		
		protected function resultHandler(event:Event):void
		{
			_asyncToken.applyGenericResult(event);	
		}
		
		
		protected function errorHandler(event:Event):void
		{
			_asyncToken.applyGenericFault(event);	
		}
		
		
	}
}
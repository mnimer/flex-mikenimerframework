package com.mikenimer.framework.adapters
{
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.XMLSocket;
	import flash.utils.Dictionary;
	
	import mx.logging.Log;
	import mx.rpc.AsyncToken;
	import mx.rpc.GenericAsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.XMLAsyncToken;
	import mx.utils.UIDUtil;
	
	public class SocketAdapter
	{
		protected var _asyncToken:GenericAsyncToken;
		protected var _socket:XMLSocket;
		protected var _host:String;
		protected var _port:int;
		protected var _available:Boolean;
		protected var _autoReconnect:Boolean;
		protected var _resp : IResponder;
		protected var tokenDict:Dictionary = new Dictionary();
		protected var msgQueue:Array = new Array();
		[Bindable]
		public function get socket() : XMLSocket
		{
			return _socket;
		}
		
		public function set socket( value : XMLSocket ) : void
		{
			_socket = value;
		}	
		
		public function establishSocket(hostName:String, port:int):void{
			_host = hostName;
			_port= port;
			_socket= new XMLSocket();
			configureListeners(_socket);
            _socket.connect(hostName, port);
  		}
		
		public function sendData(msg:XML,resp:IResponder):AsyncToken{
			
			var tokenId:String = UIDUtil.createUID();
			msg.@token = tokenId;
			var token:XMLAsyncToken = new XMLAsyncToken(msg);
			token.addResponder(resp);
			tokenDict[tokenId] = token;
			Log.getLogger("services").info("sending message:"+msg.toXMLString());
			if(_available){
				socket.send(msg.toXMLString());
			} else {
				msgQueue.push(msg);
				trace("socket not yet available, adding to queue");
			}
			return token;
		}
		private function runQueue():void{
			if(_available){
				while(msgQueue.length){
					trace("sending message:"+msgQueue.length);
					var msg:XML = msgQueue.shift();
					socket.send(msg.toXMLString());
				}
				trace("end of queue");
			}
			
		}
		protected function configureListeners(socket:XMLSocket):void{
			socket.addEventListener(Event.CLOSE, closeHandler);
            socket.addEventListener(Event.CONNECT, connectHandler);
            socket.addEventListener(DataEvent.DATA, dataHandler);
            socket.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            socket.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}
		
		protected function closeHandler(event:Event):void{
			handleDisconnect(XMLSocket(event.target));
		}
		private function handleDisconnect(socket:XMLSocket):void{
	
			_available = false;
			if(_autoReconnect){
				 tryReconnect(_socket,_host,_port);
			}
		}
		private function connectHandler(event:Event):void{
			_available = true;
			runQueue();
		}
		protected function dataHandler(event:DataEvent):void{
			var incoming:XML = XML(event.data);
			if(incoming.@token != ""){
				var tokenID:String = incoming.@token;
				var token:XMLAsyncToken = tokenDict[tokenID];
				if(token){
					token.applyGenericResult(incoming);
				}
			}
			handleIncoming(incoming);
		}
		protected function handleIncoming(msg:XML):void{
			trace("handleIncoming:"+msg.toXMLString());
			trace(msg);
			// generic handler
		}
		protected function ioErrorHandler(event:IOErrorEvent):void{
			if(!XMLSocket(event.target).connected){
				handleDisconnect(XMLSocket(event.target));
			}
			
			Log.getLogger("services").error("ioErrorHandler - " + event.text);
			
			
		}
		protected function progressHandler(event:ProgressEvent):void{
			
		}
		protected function securityErrorHandler(event:SecurityErrorEvent):void{
			if(!XMLSocket(event.target).connected){
				handleDisconnect(XMLSocket(event.target));
			}
			
			Log.getLogger("services").error("securityErrorHandler - " + event.text);
		}
		protected function tryReconnect(socket:XMLSocket,host:String, port:int):void{
			socket.connect(host,port);
		}
		public function SocketAdapter(autoReconnect:Boolean=false)
		{
			
			_autoReconnect = autoReconnect; 

		}

	}
}
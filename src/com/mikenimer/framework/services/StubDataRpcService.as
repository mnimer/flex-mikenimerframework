package com.mikenimer.framework.services
{
    import mx.core.FlexGlobals;
    import mx.core.UIComponent;
	import mx.core.mx_internal;
	import mx.messaging.messages.IMessage;
	import mx.rpc.AsyncToken;
	import mx.rpc.Fault;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	use namespace mx_internal;

	public class StubDataRpcService
	{
		private static var callLaterObject:UIComponent;
		private static var callLaterObjectPending:Boolean = true;
		private static var callLaterQueue:Array = new Array();
		
		public function result( result:Object ):AsyncToken {
			var token:AsyncToken  = new AsyncToken( null );
			var resultEvent:ResultEvent = new ResultEvent( ResultEvent.RESULT, false, true, result, token );

			callLater( simulateServerResult, [resultEvent] );

			return token;
		}
		
		public function fault( faultCode:String, faultString:String=null, detail:String=null, message:IMessage=null ):AsyncToken {
			var token:AsyncToken  = new AsyncToken( null );
			var fault:Fault = new Fault( faultCode, faultString, detail );
			var faultEvent:FaultEvent = new FaultEvent( FaultEvent.FAULT, false, true, fault, token, message );
			
			callLater( simulateServerFault, [faultEvent] );

			return token;			
		}
		
		protected function callLater( method:Function, args:Array=null ):void {
			if ( !callLaterObjectPending ) {
				callLaterObject.callLater( method, args );
			} else {
				callLaterQueue.push( { method:method, args:args } );
			}
		}

		protected function simulateServerResult( event:ResultEvent ):void {
			event.token.applyResult( event );
		}

		protected function simulateServerFault( event:FaultEvent ):void {
			event.token.applyFault( event );
		}		

		protected function waitAFrame():void
		{
			callLaterObjectPending = false;
			
			if ( callLaterQueue.length > 0 ) {
				for ( var i:int=0; i<callLaterQueue.length; i++ ) {
					callLater( callLaterQueue[i].method, callLaterQueue[i].args );
				}
				callLaterQueue = new Array();
			}
		}

		public function StubDataRpcService()
		{
			if (!callLaterObject)
			{
				callLaterObject = new UIComponent();
				callLaterObject.systemManager = FlexGlobals.topLevelApplication.systemManager;
				callLaterObject.callLater(waitAFrame);
			}
		}

	}
}
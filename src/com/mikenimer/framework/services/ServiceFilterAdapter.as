package com.mikenimer.framework.services
{
	import com.mikenimer.framework.filters.IAsyncFilter;
	import com.mikenimer.framework.filters.ServiceInvokeFilter;

	import mx.rpc.AbstractService;
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;

	public class ServiceFilterAdapter
	{
		protected var responder:IResponder;
		protected var service:*;

		public function ServiceFilterAdapter(responder:IResponder, service:*):void
		{
			this.responder = responder;
			this.service = service;
		}

		public function getFilterChain(method:String, args:Object):IAsyncFilter
		{
			var filter:IAsyncFilter = new ServiceInvokeFilter(this.service, method, args);
			//	filter = new ErrorFilter(filter);
			return filter;
		}


		public function invoke(method:String, args:Object):AsyncToken
		{
			var token:AsyncToken
			var filter:IAsyncFilter = getFilterChain(method, args);
        	token = filter.invoke();
        	token.addResponder(responder);
        	return token;
		}


	}
}
package com.mikenimer.framework.services
{
    import mx.core.FlexGlobals;
    import mx.core.UIComponent;
	import mx.core.mx_internal;
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.messages.AsyncMessage;
	import mx.messaging.messages.ErrorMessage;
	import mx.messaging.messages.IMessage;

	use namespace mx_internal;

	public class StubDataMessagingService
	{
		private static var callLaterObject:UIComponent;
		private static var callLaterObjectPending:Boolean = true;
		private static var callLaterQueue:Array = new Array();
		
		public function result(consumer:Consumer,  result:Object, headers:Object):void 
		{
			var iMessage:IMessage = new AsyncMessage(result, headers);
			var msgEvent:MessageEvent = new MessageEvent(MessageEvent.MESSAGE, false, true, iMessage);

			callLater( simulateServerResult, [msgEvent, consumer] );
		}
		
		
		public function fault(consumer:Consumer, faultCode:String, faultString:String=null, detail:String=null, message:IMessage=null ):void 
		{
			var fault:ErrorMessage = new ErrorMessage();
				fault.faultCode = faultCode;
				fault.faultString = faultString;
				fault.faultDetail = detail;
			
			callLater( simulateServerFault, [fault, consumer] );
		}

		
		protected function callLater( method:Function, args:Array=null ):void 
		{
			if ( !callLaterObjectPending ) 
			{
				callLaterObject.callLater( method, args );
			} else {
				callLaterQueue.push( { method:method, args:args } );
			}
		}


		protected function simulateServerResult( event:MessageEvent, consumer:Consumer ):void 
		{
			consumer.messageHandler(event);
		}


		protected function simulateServerFault( error:ErrorMessage, consumer:Consumer ):void 
		{
			consumer.fault(error, null);
		}		


		protected function waitAFrame():void
		{
			callLaterObjectPending = false;
			
			if ( callLaterQueue.length > 0 ) {
				for ( var i:int=0; i<callLaterQueue.length; i++ ) {
					callLater( callLaterQueue[i].method, callLaterQueue[i].args );
				}
				callLaterQueue = new Array();
			}
		}

		public function StubDataMessagingService()
		{
			if (!callLaterObject)
			{
				callLaterObject = new UIComponent();
				callLaterObject.systemManager = FlexGlobals.topLevelApplication.systemManager;
				callLaterObject.callLater(waitAFrame);
			}
		}

	}
}
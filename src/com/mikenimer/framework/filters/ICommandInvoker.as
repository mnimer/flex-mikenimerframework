package com.mikenimer.framework.filters {
	public interface ICommandInvoker {
		function getFilterChains( invoker:IAsyncFilter ):IAsyncFilter;		
	}
}
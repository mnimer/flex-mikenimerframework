/**
	Copyright (c) 2006. Tapper, Nimer and Associates Inc
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	  * Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.
	  * Redistributions in binary form must reproduce the above copyright notice,
	    this list of conditions and the following disclaimer in the documentation
	    and/or other materials provided with the distribution.
	  * Neither the name of Tapper, Nimer, and Associates nor the names of its
	    contributors may be used to endorse or promote products derived from this
	    software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
	
	@author: Mike Nimer (mikenimer@yahoo.com)
	@ignore
**/

/**
 * 
 **/
package com.mikenimer.framework.filters
{
	import mx.rpc.AsyncToken;
	import mx.rpc.AbstractService;
	import mx.rpc.AbstractOperation;
	import mx.rpc.IResponder;
	import mx.rpc.soap.WebService;
	import mx.rpc.soap.Operation;
	import mx.rpc.remoting.RemoteObject;

	public class ServiceInvokeFilter extends AbstractAsyncFilter 
	{
		private var service:AbstractService;
		private var method:String;
		private var args:Object;
		private var resultFormat:String;
		
		public function ServiceInvokeFilter(service:*, method:String, args:Object=null, resultFormat:String="e4x", nextFilter:IAsyncFilter=null)
		{
			super(nextFilter); // this has to be the last filter in the stack
			this.service = service;
			this.method = method;
			this.args = args;
			this.resultFormat = resultFormat;
		}
		
		
		
		override public function invoke():AsyncToken
		{
			var token:AsyncToken;
			
			var operation:AbstractOperation = this.service.getOperation(this.method);
			
			//trace("{ServiceInvokeFilter}.invoke() - " +operation.name +"()" );
			if( args )
			{
				operation.arguments = args;
			}
			
			// return the xml from web service calls. 
			// ideally we'd set this in the ServiceLocator (for RPC). However the WebService tag requires this to be set
			// on the operation itself.
			if( operation is mx.rpc.soap.Operation )
			{
				mx.rpc.soap.Operation(operation).resultFormat = this.resultFormat;
			}
			
			
			if( this.service is RemoteObject && args != null && !(args is Array) )
			{
				token = operation.send(args);
			}
			else
			{
				token = operation.send();
			}
		
			token.addResponder(this);
			
			return token;
		}


		override public function result(event:Object):void
		{
			//trace("{ServiceInvokeFilter}.result()");
		}


		override public function fault(event:Object):void
		{
			//trace("{ServiceInvokeFilter}.fault()");
		}
		
	}
}
package com.mikenimer.framework.filters
{
	import mx.core.mx_internal;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	use namespace mx_internal;

	public class StubDataInvokerFilter extends AbstractAsyncFilter implements IInterceptorFilter
	{
		private var asyncToken:AsyncToken = new AsyncToken( null );
		private var service:*;
		private var method:String;
		private var args:Object;

		public function get nextFilter():IAsyncFilter { return null };
		public function set nextFilter( value:IAsyncFilter ):void {};
		
		private var _data:Object;
		public function get data():Object {
			return _data;
		}

		public function set data( value:Object ):void {
			_data = value;
		}

		public function StubDataInvokerFilter(service:*, method:String, args:Object=null, resultFormat:String="e4x", nextFilter:IAsyncFilter=null)
		{
			super(nextFilter);

			this.service = service;
			this.method = method;
			this.args = args;
		}

		override public function invoke():AsyncToken
		{
			var token:AsyncToken;
			
			var operation:Function = this.service[ this.method ];
			
			//trace("{ServiceInvokeFilter}.invoke() - " +operation.name +"()" );
			if( data )
			{
				token = operation( data );
			} else {
				token = operation();
			}
			
			
			token.addResponder(this);
			
			return asyncToken;
		}

		protected function sendResult( event:ResultEvent ):void {
			asyncToken.applyResult( event );			
		}

		protected function sendFault( event:FaultEvent ):void {
			asyncToken.applyFault( event );
		}
		
		override public function result(event:Object):void
		{
			sendResult( event as ResultEvent );
		}


		override public function fault(event:Object):void
		{
			sendFault( event as FaultEvent );
		}		

	}
}
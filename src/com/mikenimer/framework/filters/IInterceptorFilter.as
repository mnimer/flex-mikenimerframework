package com.mikenimer.framework.filters
{
	import mx.rpc.IResponder;
	
	public interface IInterceptorFilter extends IAsyncFilter, IResponder
	{
		function get data():Object
		function set data( value:Object ):void;

		function get nextFilter():IAsyncFilter
		function set nextFilter( value:IAsyncFilter ):void;
	}
}
/**
	Copyright (c) 2006. Tapper, Nimer and Associates Inc
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	  * Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.
	  * Redistributions in binary form must reproduce the above copyright notice,
	    this list of conditions and the following disclaimer in the documentation
	    and/or other materials provided with the distribution.
	  * Neither the name of Tapper, Nimer, and Associates nor the names of its
	    contributors may be used to endorse or promote products derived from this
	    software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
	
	@author: Mike Nimer (mikenimer@yahoo.com)
	@ignore
**/

/**
 * 
 **/
package com.mikenimer.framework.filters
{
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.AsyncToken;
	
	public class CacheFilter extends AbstractAsyncFilter
	{
		private static var cacheManager:CacheFilter;
		private var cacheMap:Object;
		private var method:String;
		private var args:Object;
		private var cacheable:Boolean;
		private var key:String;
		
		public function CacheFilter(method:String, args:Object=null, cacheable:Boolean=false, nextFilter:IAsyncFilter=null)
		{
			super(nextFilter);
			this.method = method;
			this.args = args;
			this.cacheable = cacheable;
		}
		
		
		override public function invoke():AsyncToken
		{
			//trace("{CacheFilter}.invoke()");
			
			// check to see if cache exists, if not proceed down the execution chain.
			var token:AsyncToken;
			key = generateCacheKey(this.method, this.args);
			if( contains( key ) )
			{
				token = new AsyncToken(null);
				var resultEvt:ResultEvent = new ResultEvent("result", false, true, get(key), token);
				result(resultEvt);
			}
			else
			{
				token = this.next.invoke();
			}
			return token;
		}


		override public function result(event:Object):void
		{
			//trace("{CacheFilter}.result()");
			
			if( cacheable && event is ResultEvent )
			{
				put(key, (event as ResultEvent).result);
			}
		}


		override public function fault(event:Object):void
		{
			//trace("{CacheFilter}.fault()");
		}		
		
		
		
		
		
		
		/**
		 * Helper function to create a unique cache id, based on method event and args.
		 */
		private function generateCacheKey(method:String, args:Object):String
		{
			var cacheKey:String = method;
			if( args != null ){
				
				cacheKey += "_";
				if( ObjectUtil.isSimple(args) ){
					cacheKey += args.toString();
				} else {
					for( var key:Object in args ) {
						if(args[key] != null){
	                  		cacheKey += key+"="+args[key].toString();
	     				}	
    				 }
				}
			}
			return cacheKey;
		}
				
		
		private function contains(key:Object):Boolean
		{
			return cacheMap.hasOwnProperty(key);
		}
		
		
		private function put(key:Object, value:Object):Object
		{
			// store a wrapper object, so we can clean up the cache based on time. 
			var wrapperObject:Object = new Object();
			wrapperObject.timestamp = new Date().getTime();
			wrapperObject.value = value;
			
			var previousValue:Object = this.cacheMap[key];
			this.cacheMap[key] = wrapperObject;
			return previousValue!=null?previousValue.value:null;
		}
		
		
		private function get(key:Object):Object
		{
			if( !contains(key) )
			{
				throw new Error( key +" does not exists in the cache");
			}
			//return the "value" property of the wrapper object.
			return this.cacheMap[key].value;
		}
		
		
		private function remove(key:Object):Object
		{
			var val:Object = get(key);
			delete this.cacheMap[key];
			return val;
		}
		
	}
}

/**
	Copyright (c) 2006. Tapper, Nimer and Associates Inc
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	  * Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.
	  * Redistributions in binary form must reproduce the above copyright notice,
	    this list of conditions and the following disclaimer in the documentation
	    and/or other materials provided with the distribution.
	  * Neither the name of Tapper, Nimer, and Associates nor the names of its
	    contributors may be used to endorse or promote products derived from this
	    software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
	
	@author: Mike Nimer (mikenimer@yahoo.com)
	@ignore
**/

/**
 * 
 **/
package com.mikenimer.framework.filters
{
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	public class PendingQueueFilter extends AbstractAsyncFilter
	{
		private static var queueMap:Dictionary;
		
		private var cleanupTimer:Timer;
		private var timeToLive:int = 60000;
		private var key:String;
		private var method:String;
		private var args:Object;
		
		
		public function PendingQueueFilter(method:String, args:Object=null, nextFilter:IAsyncFilter=null)
		{
			super(nextFilter);
			this.method = method;
			this.args = args;

			if( queueMap == null )
			{
				queueMap = new Dictionary();
				cleanupTimer = new Timer(timeToLive, 0); //run every minute.
				cleanupTimer.addEventListener(TimerEvent.TIMER, cleanUp);
				cleanupTimer.start();
			}
		}
		
		
		override public function invoke():AsyncToken
		{
			//trace("{PendingQueueFilter}.invoke()");
			
			// check to see if cache exists, if not proceed down the execution chain.
			var token:AsyncToken;
			key = generateCacheKey(this.method, this.args);
			if( contains( key ) )
			{
				token = new AsyncToken(null);
				var resultEvt:ResultEvent = new ResultEvent("result", false, true, get(key), token);
				result(resultEvt);
			}
			else
			{
				token = this.next.invoke();
			}
			return token;
		}


		override public function result(event:Object):void
		{
			//trace("{PendingQueueFilter}.result()");
			
			if( event is ResultEvent )
			{
				put(key, (event as ResultEvent).result);
			}
		}


		override public function fault(event:Object):void
		{
			//trace("{PendingQueueFilter}.fault()");
		}		
		
		
				
		
		/**
		 * Loop over the existing key items and if they are older then the timeToLive (1 min)
		 * remove them from the pendingQueueManager.
		 */
		private function cleanUp(event:TimerEvent):void
		{
			var now:int = new Date().getTime();
			for( var key:Object in queueMap )
			{
				var item:Object = queueMap[key];
				if( (now - item.timestamp) > timeToLive )
				{
					delete queueMap[key];	
				}
			}
		}
		
		
		/**
		 * Helper function to create a unique cache id, based on method event and args.
		 */
		private function generateCacheKey(method:String, args:Object):String
		{
			var cacheKey:String = method;
			if( args != null ){
				
				cacheKey += "_";
				if( ObjectUtil.isSimple(args) ){
					cacheKey += args.toString();
				} else {
					for( var key:Object in args ) {
						if(args[key] != null){
	                  		cacheKey += key+"="+args[key].toString();
	     				}	
    				 }
				}
			}
			return cacheKey;
		}
		
		
		private function contains(key:Object):Boolean
		{
			return queueMap.hasOwnProperty(key);
		}
		
		
		private function put(key:Object, value:Object):Array
		{
			// special check so we can return for previous item, if this is the 1st item added to the queue.
			var isNull:Boolean = false;
			var previousValue:Object = PendingQueueFilter.queueMap[key];
			if( previousValue == null )
			{
				isNull=true; 
				// store a wrapper object, so we can clean up the cache based on time. 
				previousValue = new Object();
				previousValue.timestamp = new Date().getTime();
				previousValue.value = [];
			}
			
			previousValue.value[(previousValue.value as Array).length] = value;
			
			PendingQueueFilter.queueMap[key] = previousValue;
			
			if( isNull )
			{
				return null;
			}
			return previousValue.value;
		}
		
		
		private function get(key:Object):Array
		{
			if( !contains(key) )
			{
				return null;
			}
			//return the "value" property of the wrapper object.
			return PendingQueueFilter.queueMap[key].value;
		}
		
		
		private function remove(key:Object):Array
		{
			var val:Array = get(key);
			delete PendingQueueFilter.queueMap[key];
			return val;
		}
		
	}
}
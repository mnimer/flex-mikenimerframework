/**
	Copyright (c) 2006. Tapper, Nimer and Associates Inc
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	  * Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.
	  * Redistributions in binary form must reproduce the above copyright notice,
	    this list of conditions and the following disclaimer in the documentation
	    and/or other materials provided with the distribution.
	  * Neither the name of Tapper, Nimer, and Associates nor the names of its
	    contributors may be used to endorse or promote products derived from this
	    software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
	
	@author: Mike Nimer (mikenimer@yahoo.com)
	@ignore
**/

/**
 * 
 **/
package com.mikenimer.framework.filters
{

	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import com.mikenimer.framework.filters.AbstractAsyncFilter;
	import com.mikenimer.framework.filters.IAsyncFilter;
	

	/**
	 * All requests will go through this filter, this way if the users
	 * session is timed out, or the user is no loner authorized, we can send them 
	 * right back to the login screen. 
	 * 
	 * @author: Mike Nimer
	 **/
	public class AuthenticationFilter extends AbstractAsyncFilter
	{
		private var token:AsyncToken;
		public function AuthenticationFilter(nextFilter:IAsyncFilter)
		{
			super(nextFilter);
		}
		
		
		override public function invoke():AsyncToken
		{
			//trace("{AuthenticationFilter}.invoke()");
			token = this.next.invoke();
			token.addResponder(this);
			return token;
		}


		override public function result(event:Object):void
		{
			//trace("{AuthenticationFilter}.result()");
		}


		/**
		 * if the error is an Session timed out, or user not authenticated. 
		 * Send user back to the login form
		 **/
		override public function fault(event:Object):void
		{
			//trace("{AuthenticationFilter}.fault()");
			if( event is FaultEvent 
				&& (event as FaultEvent).fault.faultString.toLowerCase().match("not authorized") != null 
				&& (event as FaultEvent).fault.faultString.toLowerCase().match("login incorrect") != null )
			{
				//todo, something when their is an authentication error.
				
				// stop the fault chain.
				(event as FaultEvent).preventDefault();
			}
		}
		
	}
}
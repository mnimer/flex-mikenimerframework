package com.mikenimer.framework.command
{
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	
	public interface ICommand extends IResponder
	{
		function execute():void;
		function get asyncToken():AsyncToken;
		function set asyncToken( value:AsyncToken ):void;
	}
}
package com.mikenimer.framework.command
{
	import com.adobe.utils.StringUtil;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.Responder;
	
	import mx.collections.ArrayCollection;
	import mx.controls.DateField;
	import mx.core.mx_internal;
	import mx.rpc.AsyncToken;
	import mx.rpc.GenericAsyncToken;
	import mx.rpc.events.ResultEvent;
	use namespace mx_internal;

	/**
	 * NetConnection Command Adapter that knows how to turn NC.call commands into commands that use AsynToken. 
	 **/
	public class GenericResponderCommandAdapter extends Responder implements ICommand
	{
		private var _asyncToken:GenericAsyncToken;
		protected var _resultSource:Object;
		protected var _resultProperty:Object;
		protected var _resultCallback:Function;
		protected var _faultCallback:Function;
		protected var _broadcaster:EventDispatcher;
		protected var _event:Event;
		protected var _eventResultProperty:String;

		public function execute():void
		{			
			throw new Error("You must override execute with your own method");
		}
		
		
		public function get asyncToken():AsyncToken
		{
			return _asyncToken;
		}
		
		
		public function set asyncToken(value:AsyncToken):void
		{
			_asyncToken = value as GenericAsyncToken;
		}
 

		public function setResult(resultSource_:Object = null, resultProperty_:Object = null):GenericResponderCommandAdapter 
		{			
			this._resultSource = resultSource_;
			this._resultProperty = resultProperty_;			
			
			return this;
		}
		
 
		public function setCallbacks(resultCallback_:Function = null, faultCallback_:Function = null):GenericResponderCommandAdapter
		{			
			this._resultCallback = resultCallback_;
			this._faultCallback = faultCallback_;
			
			return this;
		}
		

		public function setBroadcastEvent(broadcaster:EventDispatcher, event:Event, resultProperty:String=null): GenericResponderCommandAdapter 
		{
			this._broadcaster = broadcaster;
			this._event = event;
			this._eventResultProperty = resultProperty;
			
			return this;
		}		


 		public function result(data:Object):void
		{
			asyncToken.applyGenericResult( data );
			
			// handle the calls to setResult()
			handleResult(data);			
			
			// handle calls to setBroadcastEvent()
			if (_event != null && _broadcaster != null && data is ResultEvent ) 
			{ 
				if( _eventResultProperty != null )
				{
					_event[_eventResultProperty] = convert(_event[_eventResultProperty], ResultEvent(data).result);
				}
				_broadcaster.dispatchEvent(_event); 
			}

			// handle calls to setCallback()
			if (_resultCallback != null) { 
				_resultCallback.call(this, data); 
			}
		}
		
		public function fault(info:Object):void
		{
			asyncToken.applyGenericFault( info );
			
			if (_faultCallback != null) {
				_faultCallback.call(this, info); 
			}
		}		
		
		
		private function handleResult(data:Object):void
		{
			if (_resultSource != null && _resultProperty != null )
			{
				if( _resultSource[_resultProperty] != null )
				{
					if( data is ResultEvent)
					{
						_resultSource[_resultProperty] = convert(_resultSource[_resultProperty], ResultEvent(data).result);
					}
				}
				
				try
				{
					if( data is ResultEvent)
					{
						_resultSource[_resultProperty] = ResultEvent(data).result;
					}
				}catch( err:Error ){
				
					if( err.errorID == 1034 )
					{
						var parts:Array = err.message.split(" ");
						var type:String = parts[parts.length-1];
						if( StringUtil.endsWith(type, ".") )
						{
							type = type.substring(0, type.length-1);
						}
						
						if( parts.length > 2 )
						{
							_resultSource[_resultProperty] = convert(type, ResultEvent(data).result);
						}
						else if( ResultEvent(data).result is Array )
						{
							_resultSource[_resultProperty] = new ArrayCollection(ResultEvent(data).result as Array);
						}
						else
						{
							_resultSource[_resultProperty] = ResultEvent(data).result;
						}
					}
				}
			}
		}
		
		
		protected function convert(source:Object , result:Object):Object
		{
			if( source is ArrayCollection || (source is String && source == "mx.collections.ArrayCollection") )
			{
				if(result is Array )
				{ 
					return new ArrayCollection(result as Array); 
			    }
			}
			else if( source is Boolean )
			{
				if(result is String )
				{ 
					if( result.toString() == "true" || result.toString() == "1" )
					{
						return true;
					}else{
						return false
					}
			    }
			}
			else if( source is Date )
			{
				if(result is String )
				{ 
					// todo, find a more generic way to parse date objects of any format
					DateField.stringToDate(result.toString(), "mm/dd/yyyy");
			    }
			}
			else if( source is String )
			{
				return result.toString();
			}
			
			return result; 			
		}
		

		public function GenericResponderCommandAdapter( result:Function=null, fault:Function=null )
		{
			super(result, fault);
			_asyncToken = new GenericAsyncToken( null );
		}
	}
}

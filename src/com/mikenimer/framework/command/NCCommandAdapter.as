package com.mikenimer.framework.command
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.core.mx_internal;
	import mx.rpc.AsyncToken;
	import mx.rpc.GenericAsyncToken;
	use namespace mx_internal;

	/**
	 * NetConnection Command Adapter that knows how to turn NC.call commands into commands that use AsynToken. 
	 **/
	public class NCCommandAdapter extends GenericResponderCommandAdapter
	{
		public function NCCommandAdapter()
		{
			super(result, fault);
		}
	}
}

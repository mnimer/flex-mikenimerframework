package com.mikenimer.framework.command
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.Responder;
	import flash.utils.*;
	
	import mx.core.mx_internal;
	import mx.rpc.AsyncToken;
	import mx.rpc.GenericAsyncToken;
	import mx.utils.StringUtil;
	import flash.errors.SQLError;
	import flash.events.SQLEvent;
	import mx.events.CloseEvent;
	use namespace mx_internal;

	public class SQLCommand extends Responder implements ICommand
	{
		protected static var sqlStatementClassReference:Class;

		private var _asyncToken:GenericAsyncToken;
		protected var _resultSource:Object;
		protected var _resultProperty:Object;
		protected var _resultCallback:Function;
		protected var _faultCallback:Function;
		protected var _broadcaster:EventDispatcher;
		protected var _event:Event;
		protected var _eventResultProperty:String;

        // typeless so this can be compiled into a flex app too.
		protected var connection:*;
		protected var sqlStatement:*;

		public function execute():void {
		}
		
		public function get asyncToken():AsyncToken
		{
			return _asyncToken;
		}
		
		public function set asyncToken(value:AsyncToken):void
		{
			_asyncToken = value as GenericAsyncToken;
		}

		
		public function setResult(resultSource_:Object = null, resultProperty_:Object = null):SQLCommand 
		{			
			this._resultSource = resultSource_;
			this._resultProperty = resultProperty_;			
			
			return this;
		}
		
		
		public function setCallbacks(resultCallback_:Function = null, faultCallback_:Function = null):SQLCommand
		{			
			this._resultCallback = resultCallback_;
			this._faultCallback = faultCallback_;
			
			return this;
		}
		
		
/* 		
		Not yet supported
		public function setBroadcastEvent(broadcaster:EventDispatcher, event:Event, resultProperty:String=null): SQLCommand 
		{
			this._broadcaster = broadcaster;
			this._event = event;
			this._eventResultProperty = resultProperty;
			
			return this;
		}		

 */
 		public function result(data:Object):void
		{
			asyncToken.applyGenericResult( data );
			
			if (_resultCallback != null) { 
				_resultCallback.call(this, data); 
			}
			
			if( this._resultSource != null && this._resultProperty != null )
			{
			    this._resultSource[this._resultProperty] = sqlStatement.getResult();
			}
			
			if( connection )
			{
			    connection.close();
			}
			
		}
		
		public function fault(info:Object):void
		{
			asyncToken.applyGenericFault( info );
			
			if (_faultCallback != null) {
				_faultCallback.call(this, info); 
			}
			
			connection.close();
		}		

		public function SQLCommand( connection_:*)
		{
			super(result, fault);

/*			
			if ( !sqlStatementClassReference ) {
				sqlStatementClassReference = getDefinitionByName("flash.data.SQLStatement") as Class;
				if ( !sqlStatementClassReference ) {
					throw new Error( 'AIR Libraries not included but SQLCommand being used' );
				}
			} 
*/
			this.connection = connection_;

/*			statement = new sqlStatementClassReference();
			statement.sqlConnection = this.connection;
			statement.text = sqlText;
*/
			_asyncToken = new GenericAsyncToken( null );
		}
	}
}

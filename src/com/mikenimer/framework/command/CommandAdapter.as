package com.mikenimer.framework.command
{
	import com.adobe.utils.StringUtil;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.DateField;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	
	public class CommandAdapter implements ICommand
	{
		protected var _resultSource:Object;
		protected var _resultProperty:Object;
		protected var _resultCallback:Function;
		protected var _faultCallback:Function;
		protected var _broadcaster:EventDispatcher;
		protected var _event:Event;
		protected var _eventResultProperty:String;
		
		private var _asyncToken:AsyncToken;
		
		public function get asyncToken():AsyncToken {
			return _asyncToken;
		}
		
		public function set asyncToken( value:AsyncToken ):void {
			_asyncToken = value;
		}

		public function setResult(resultSource_:Object = null, resultProperty_:Object = null):CommandAdapter 
		{			
			this._resultSource = resultSource_;
			this._resultProperty = resultProperty_;			
			
			return this;
		}
		

		public function setCallbacks(resultCallback_:Function = null, faultCallback_:Function = null):CommandAdapter
		{			
			this._resultCallback = resultCallback_;
			this._faultCallback = faultCallback_;
			
			return this;
		}
		
		
		public function setBroadcastEvent(broadcaster:EventDispatcher, event:Event, resultProperty:String=null): CommandAdapter 
		{
			this._broadcaster = broadcaster;
			this._event = event;
			this._eventResultProperty = resultProperty;
			
			return this;
		}		
				
		
		public function execute():void {}
		

		public function result(event:Object):void
		{
			if (_resultSource != null && _resultProperty != null )
			{
				if( _resultSource[_resultProperty] != null )
				{
					_resultSource[_resultProperty] = convert(_resultSource[_resultProperty], event.result);
				}
				
				try
				{
					_resultSource[_resultProperty] = event.result;
				}catch( err:Error ){
				
					if( err.errorID == 1034 )
					{
						var parts:Array = err.message.split(" ");
						var type:String = parts[parts.length-1];
						if( StringUtil.endsWith(type, ".") )
						{
							type = type.substring(0, type.length-1);
						}
						
						if( parts.length > 2 )
						{
							_resultSource[_resultProperty] = convert(type, event.result);
						}
						else if( event.result is Array )
						{
							_resultSource[_resultProperty] = new ArrayCollection(event.result);
						}
						else
						{
							_resultSource[_resultProperty] = event.result;
						}
					}
				}
			}
			
			if (_event != null && _broadcaster != null ) 
			{ 
				if( _eventResultProperty != null )
				{
					_event[_eventResultProperty] = convert(_event[_eventResultProperty], event.result);
				}
				_broadcaster.dispatchEvent(_event); 
			}

			
			if (_resultCallback != null) { 
				_resultCallback.call(this, event); 
			}
		}
		

		public function fault(event:Object):void
		{
			if (_faultCallback != null) {
				_faultCallback.call(this, event); 
			}
			
			
		}
		
		
		protected function convert(source:Object , result:Object):Object
		{
			if( source is ArrayCollection || (source is String && source == "mx.collections.ArrayCollection") )
			{
				if(result is Array )
				{ 
					return new ArrayCollection(result as Array); 
			    }
			}
			else if( source is Boolean )
			{
				if(result is String )
				{ 
					if( result.toString() == "true" || result.toString() == "1" )
					{
						return true;
					}else{
						return false
					}
			    }
			}
			else if( source is Date )
			{
				if(result is String )
				{ 
					// todo, find a more generic way to parse date objects of any format
					DateField.stringToDate(result.toString(), "mm/dd/yyyy");
			    }
			}
			else if( source is String )
			{
				return result.toString();
			}
			
			return result; 			
		}
		
		public function CommandAdapter() {
			_asyncToken = new AsyncToken( null );
		}
	}
}
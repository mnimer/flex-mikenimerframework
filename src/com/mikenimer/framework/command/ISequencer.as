package com.mikenimer.framework.command
{
	public interface ISequencer extends ICommand
	{
		function get queue():Array;
		function set queue( value:Array ):void;
		
		
		function pauseSequence():ICommand;
		function resumeSequence():void;
		function stopSequence():ICommand;
	}
}
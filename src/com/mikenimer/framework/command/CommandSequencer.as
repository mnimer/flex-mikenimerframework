package com.mikenimer.framework.command
{
	import mx.rpc.AsyncToken;
	import mx.rpc.GenericAsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	
	public class CommandSequencer implements IResponder
	{
		private var _asyncToken:GenericAsyncToken;
		private var queue:Array = [];
		private var invokeCounter:int=0;
		protected var _resultCallback:Function;
		protected var _faultCallback:Function;
		protected var throwException:Boolean = false;

		public function add(item:ICommand):Object
		{
			queue.push(item);	
			return this;
		}

		public function setCallbacks(resultCallback_:Function = null, faultCallback_:Function = null):CommandSequencer
		{			
			this._resultCallback = resultCallback_;
			this._faultCallback = faultCallback_;
			
			return this;
		}

		public function execute():void
		{
			if( invokeCounter < queue.length  )
			{
				var c:ICommand = ICommand(queue[invokeCounter++]);
					c.execute();
					c.asyncToken.addResponder(this);
			}
			else
			{
				invokeCounter = 0;
			}
		}
		
		public function result(data:Object):void
		{
			execute();

			if( invokeCounter >= queue.length  ) {
				asyncToken.applyGenericResult( data );

				if (_resultCallback != null) { 
					_resultCallback.call(this, data); 
				}
			}
		}
		
		public function fault(info:Object):void
		{
			var count:int = invokeCounter-1;
			invokeCounter = 0;

			asyncToken.applyGenericFault( info );

			if (_faultCallback != null) {
				_faultCallback.call(this, info); 
			}
		
			if( throwException )
			{	
				if ( info is FaultEvent ) {
					throw new Error("Error invoking command: " +count +"\n" +info.fault.faultString);
				} else {
					throw new Error("Error invoking command: " +count +"\n");
				}
			}
		}

		public function get asyncToken():AsyncToken
		{
			return _asyncToken;
		}
		
		public function set asyncToken(value:AsyncToken):void
		{
			_asyncToken = value as GenericAsyncToken;
		}
		
		public function CommandSequencer(throwException:Boolean=false) 
		{
			this.throwException = throwException;
			_asyncToken = new GenericAsyncToken( null );
		}
	}
}